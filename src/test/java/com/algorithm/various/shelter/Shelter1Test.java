package com.algorithm.various.shelter;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;


public class Shelter1Test {
    @Test
    public void adoptAnyForEmptyShelterReturnsNull() {
        //When
        Shelter1 shelter = new Shelter1();
        //Given
        AbstractAnimal animal = shelter.adoptAny();
        //Then
        assertThat(shelter.size()).isEqualTo(0);
        assertThat(shelter.hasAnimals()).isFalse();
        assertThat(animal).isNull();
    }

    @Test
    public void adoptAnyReturnsTheOldestDog() {
        //When
        Shelter1 shelter = new Shelter1();
        Dog dog = new Dog();
        //Given
        shelter.enqueue(dog);
        shelter.enqueue(new Dog());
        shelter.enqueue(new Dog());
        shelter.enqueue(new Dog());
        shelter.enqueue(new Dog());

        //Then

        assertThat(shelter.size()).isPositive();
        assertThat(shelter.size()).isEqualTo(5);
        assertThat(shelter.hasAnimals()).isTrue();
        assertThat(shelter.adoptAny()).isEqualTo(dog);
    }

    @Test
    public void adoptAnyReturnsTheOldestCat() {
        //When
        Shelter1 shelter = new Shelter1();
        Cat cat = new Cat();
        //Given
        shelter.enqueue(cat);
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());

        //Then

        assertThat(shelter.size()).isPositive();
        assertThat(shelter.size()).isEqualTo(5);
        assertThat(shelter.hasAnimals()).isTrue();
        assertThat(shelter.adoptAny()).isEqualTo(cat);
    }

    @Test
    public void adoptCatReturnsCat() {
        //When
        Shelter1 shelter = new Shelter1();
        Cat cat = new Cat();
        //Given
        shelter.enqueue(new Dog());
        shelter.enqueue(new Dog());
        shelter.enqueue(cat);
        shelter.enqueue(new Dog());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        //Then
        AbstractAnimal animal = shelter.adoptCat();

        assertThat(animal).isNotNull();
        assertThat(animal).isInstanceOf(Cat.class);
    }

    @Test
    public void adoptDogReturnsDog() {
        //When
        Shelter1 shelter = new Shelter1();
        //Given
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Cat());
        shelter.enqueue(new Dog());
        //Then
        AbstractAnimal animal = shelter.adoptDog();

        assertThat(animal).isNotNull();
        assertThat(animal).isInstanceOf(Dog.class);
    }
}