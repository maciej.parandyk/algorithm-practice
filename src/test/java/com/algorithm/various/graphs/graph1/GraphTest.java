package com.algorithm.various.graphs.graph1;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.*;

public class GraphTest {

    @Test
    public void nodeShouldBeFound() {
        //When
        Graph graph = new Graph();
        Node start = new Node();
        Node end = new Node();
        //Given
        createConnectedGraph(graph, start, end);
        //Then

        assertThat(graph.areNodesConnected(start, end)).isTrue();
    }

    private void createConnectedGraph(Graph graph, Node from, Node to) {
        Node n1 = new Node();
        from.addAdjacent(new Node());
        Node node2 = new Node();
        from.addAdjacent(node2);
        n1.addAdjacent(new Node());
        n1.addAdjacent(new Node());
        n1.addAdjacent(new Node());
        Node no3 = new Node();
        node2.addAdjacent(no3);
        node2.addAdjacent(new Node());
        node2.addAdjacent(new Node());
        Node n4 = new Node();
        no3.addAdjacent(n4);
        n4.addAdjacent(new Node());
        n4.addAdjacent(to);
        n4.addAdjacent(new Node());
        graph.getNodes().add(from);
        graph.getNodes().add(to);
        graph.getNodes().add(new Node());
    }

    @Test
    public void nodeShouldNotBeFound() {
        //When
        Graph graph = new Graph();
        Node start = new Node();
        Node end = new Node();
        //Given
        createConnectedGraph(graph, new Node(), end);
        //Then

        assertThat(graph.areNodesConnected(start, end)).isFalse();
    }
}