package com.algorithm.sort.quick.array;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayQuickSortTest {private static final String MAIN_PROVIDER = "main_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[] getData(){
        return new Object[][]{
                {85,12,0,2,18,315,3,12},
                {1,15,28,22,13,14,16},
                {45,1022,23,30,45,78,0,2,3,5,64},
                {13,0,5,78,63,40,15,12,4,9,3,3,33,2,11},
        };
    }


    @Test(dataProvider = MAIN_PROVIDER)
    public void arraySortTest(Integer[] args) {
        //When
        final Integer[] cpy = Arrays.copyOf(args, args.length);
        Arrays.sort(cpy);
        //Given
        ArrayQuickSort.sort(args);
        //Then
        assertThat(args).containsExactly(cpy);
        assertThat(args).isEqualTo(cpy);
    }
}