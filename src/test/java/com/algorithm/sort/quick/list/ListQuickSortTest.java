package com.algorithm.sort.quick.list;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ListQuickSortTest {
    private static final String MAIN_PROVIDER = "main_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[] getData(){
        return new Object[][]{
                {85,12,0,2,18,315,3,12},
                {1,15,28,22,13,14,16},
                {45,1022,23,30,45,78,0,2,3,5,64},
                {13,0,5,78,63,40,15,12,4,9,3,3,33,2,11},
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void listSortTest(Integer[] args) {
        //When
        List<Integer> lista = new ArrayList(Arrays.asList(args));
        final List<Integer> expected = new ArrayList(Arrays.asList(args));
        expected.sort(Comparator.naturalOrder());
        //Given
        ListQuickSort.sort(lista);
        //Then

        assertThat(lista).isEqualTo(expected);
    }
}