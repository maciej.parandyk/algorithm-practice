package com.algorithm.sort.insert.array;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayInsertSortTest {
    private static final String MAIN_PROVIDER = "main_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[] getData(){
        return new Object[][]{
                {85,12,0,2,18,315,3,12},
                {1,15,28,22,13,14,16}
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void arraySortTest(Integer[] args) {
        //When
        final Integer[] cpy = Arrays.copyOf(args, args.length);
        Arrays.sort(cpy);
        //Given
        ArrayInsertSort.sort(args);
        //Then

        assertThat(args).containsExactly(cpy);
        assertThat(args).isEqualTo(cpy);
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void listSortTest(Integer[] args) {
        //When
        List<Integer> lista = new ArrayList(Arrays.asList(args));
        final List<Integer> expected = new ArrayList(Arrays.asList(args));
        expected.sort(Comparator.naturalOrder());
        //Given
        ArrayInsertSort.sort(lista);
        //Then

        assertThat(lista).isEqualTo(expected);
    }
}