package com.algorithm.sort.median;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;

public class ArrayMedianSortTest {
    private static final String MAIN_PROVIDER = "main_data_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[][] getProv() {
        return new Object[][]{
                {15, 6, 0, 12, 4, 6, 8, 19},
                {45, 0, 12, 4, 7, 8, 9, 1, 2, 3, 12, 15, 2}
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void sortTest(Integer[] values) {
        //When
        final Integer[] expected = Arrays.copyOf(values, values.length);
        //Given
        Arrays.sort(expected);
        //Then
        ArrayMedianSort.sort(values);

        assertThat(values).isEqualTo(expected);
    }
}