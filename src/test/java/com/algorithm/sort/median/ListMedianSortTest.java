package com.algorithm.sort.median;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ListMedianSortTest {
    private static final String MAIN_PROVIDER = "main_data_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[][] getProv(){
        return new Object[][]{
                {15,6,0,12,4,6,8,19},
                {45,0,12,4,7,8,9,1,2,3,12,15,2}
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void sortTest(int[] values) {
        //When
        final List<Integer> expected = new ArrayList<>();
        final List<Integer> list = new ArrayList<>();
        for (int value : values) {
            expected.add(value);
            list.add(value);
        }
        //Given
        expected.sort(Comparator.naturalOrder());
        //Then
        ListMedianSort.sort(list);
        assertThat(list).isEqualTo(expected);
    }

}