package com.algorithm.sort.selection.array;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ListSelectionSortTest {
    private static final String MAIN_PROVIDER = "main_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[] getData(){
        return new Object[][]{
                {85,12,0,2,18,315,3,12},
                {1,15,28,22,13,14,16}
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void listSortTest(Integer[] args) {
        //When
        List<Integer> lista = new ArrayList(Arrays.asList(args));
        final List<Integer> expected = new ArrayList(Arrays.asList(args));
        expected.sort(Comparator.naturalOrder());
        //Given
        ListSelectionSort.sort(lista);
        //Then

        assertThat(lista).isEqualTo(expected);
    }
}