package com.algorithm.sort.merge.array;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class MergeSortTest {
    private static final String MAIN_PROVIDER = "main_provider";

    @DataProvider(name = MAIN_PROVIDER)
    Object[] getData(){
        return new Object[][]{
                {85,12,0,2,18,315,3,12},
                {1,15,28,22,13,14,16}
        };
    }

    @Test(dataProvider = MAIN_PROVIDER)
    public void arraySortTest(Integer[] args) {
        //When
        final Integer[] cpy = Arrays.copyOf(args, args.length);
        Arrays.sort(cpy);
        //Given
        MergeSort.sort(args);
        //Then

        assertThat(args).containsExactly(cpy);
        assertThat(args).isEqualTo(cpy);
    }

}