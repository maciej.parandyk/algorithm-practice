package com.algorithm.various.shelter;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class Shelter1 {
    private final Queue<AbstractAnimal> dogs;
    private final Queue<AbstractAnimal> cats;

    public Shelter1() {
        dogs = new LinkedList<>();
        cats = new LinkedList<>();
    }

    void enqueue(AbstractAnimal animal) {
        if (!Objects.isNull(animal)) {
            if (animal instanceof Cat) {
                cats.add(animal);
            } else if (animal instanceof Dog) {
                dogs.add(animal);
            }
        }
    }

    public AbstractAnimal adoptAny() {
        if (!cats.isEmpty() && cats.peek().compareTo(dogs.peek()) <= 0) {
            return adoptCat();
        } else {
            return adoptDog();
        }
    }

    public Dog adoptDog() {
        return (Dog) dogs.poll();
    }

    public Cat adoptCat() {
        return (Cat) cats.poll();
    }

    public int size() {
        return cats.size() + dogs.size();
    }

    public boolean hasAnimals() {
        return size() != 0;
    }
}
