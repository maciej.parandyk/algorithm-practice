package com.algorithm.various.shelter;

public class Cat extends AbstractAnimal {

    public Cat() {
        super();
    }

    @Override
    public int hashCode() {
        return timeAdded.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Cat)) {
            return false;
        }

        Cat cat = (Cat) obj;

        return timeAdded.equals(cat.timeAdded);
    }
}
