package com.algorithm.various.shelter;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class AbstractAnimal implements Comparable<AbstractAnimal> {
    protected final LocalDateTime timeAdded;

    public AbstractAnimal() {
        this.timeAdded = LocalDateTime.now();
    }

    @Override
    public int compareTo(AbstractAnimal ab) {
        if (Objects.isNull(ab)) {
            return -1;
        }

        return timeAdded.compareTo(ab.timeAdded);
    }

    public abstract int hashCode();

    public abstract boolean equals(Object obj);
}
