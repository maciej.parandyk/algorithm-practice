package com.algorithm.various.shelter;

public class Dog extends AbstractAnimal {
    public Dog() {
        super();
    }

    @Override
    public int hashCode() {
        return timeAdded.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Dog)) {
            return false;
        }

        Dog dog = (Dog) obj;

        return timeAdded.equals(dog.timeAdded);
    }
}
