package com.algorithm.various.graphs.build.build2;

import com.algorithm.various.graphs.graph1.Graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph2 {
    private final Map<String, Project> nodes;

    public Graph2() {
        this.nodes = new HashMap<>();
    }

    public void addNodes(String[][] nodes) {
        for(String[] arg : nodes) {
            Project p1 = getProject(arg[0]);
            Project p2 = getProject(arg[1]);
            p1.addDependency(p2);
        }
    }

    public List<Project> getNodes() {
        return new ArrayList<>(nodes.values());
    }

    private Project getProject(String name) {
        return nodes.computeIfAbsent(name, Project::new);
    }
}
