package com.algorithm.various.graphs.build.build1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BuildProcess1 {

    private BuildProcess1() {}

    public static Project[] orderChain(List<Project> projects) {
        Project[] orderArray = new Project[projects.size()];

        int start = getElements(projects, orderArray, 0);

        int idx = 0;

        int size = orderArray.length;
        while (idx < size) {
            Project project = orderArray[idx];

            if (Objects.isNull(project)) {
                return new Project[0];
            }

            for (Project projs : project.getDependencies()) {
                projs.decreaseDependent();
            }

            start = getElements(projects, orderArray, start);
            idx++;
        }

        return orderArray;
    }

    private static int getElements(List<Project> projects, Project[] orderArray, int start) {
        List<Project> tmp = new ArrayList<>();
        for (Project project: projects) {
            if (project.dependens() == 0) {
                orderArray[start++] = project;
                tmp.add(project);
            }
        }

        projects.removeAll(tmp);

        return start;
    }

    public static void main(String[] args) {
        Graph graph = new Graph();
        String[][] dependencies = {
                {"d", "g"}, {"f", "c"}, {"f","b"}, {"c","a"}, {"b", "a"}, {"b", "e"}, {"a", "e"}
        };

        graph.addProducts(dependencies);
        List<Project> projects = graph.getList();

        Project[] order = orderChain(projects);

        if (order.length == 0) {
            System.out.println("Sorry");
        } else {
            System.out.println("Build order");
            for (Project project : order) {
                System.out.println("\t"+project);
            }
        }

    }
}
