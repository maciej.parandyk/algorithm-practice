package com.algorithm.various.graphs.build.build2;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class Build {

    public static Stack<Project> getBuildOrder(Collection<Project> nodes) {
        Stack<Project> list = new Stack<>();

        for (Project p: nodes) {
            if (p.getState() == ProjectState.BLANK) {
                if (!doDFS(p, list)) {
                    return new Stack<>();
                }

            }
        }

        return list;
    }


    private static boolean doDFS(Project project, Stack<Project> stack) {
        if (project.getState() == ProjectState.VISITING) {
            return false;
        }

        if (project.getState() == ProjectState.BLANK) {
            project.setState(ProjectState.VISITING);

            for (Project p: project.getDependencies()) {
                if (!doDFS(p, stack)) {
                    return false;
                }
            }

            project.setState(ProjectState.VISITED);
            stack.push(project);
        }

        return true;
    }

    public static void main(String[] args) {
        Graph2 graph = new Graph2();
        String[][] dependencies = {
                {"d", "g"}, {"f", "c"}, {"f","b"}, {"c","a"}, {"b", "a"}, {"b", "e"}, {"a", "e"}
        };

        graph.addNodes(dependencies);

        List<Project> nodes = graph.getNodes();

        Stack<Project> result = getBuildOrder(nodes);

        System.out.println(result);

        for (Project p: result) {
            System.out.println(p);
        }
    }
}
