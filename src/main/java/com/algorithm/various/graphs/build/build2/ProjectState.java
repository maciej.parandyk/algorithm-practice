package com.algorithm.various.graphs.build.build2;

public enum ProjectState {
    BLANK,
    VISITING,
    VISITED
}
