package com.algorithm.various.graphs.build.build1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Project {
    private final String name;
    private int dependentAmount;
    private final List<Project> dependencies;

    public Project(String name) {
        this.name = name;
        this.dependentAmount = 0;
        this.dependencies = new ArrayList<>();
    }

    public int dependens() {
        return dependentAmount;
    }

    public void decreaseDependent() {
        this.dependentAmount--;
    }

    void incrementDependency() {
        this.dependentAmount++;
    }

    public void addDependency(Project project) {
        this.dependencies.add(project);
        project.incrementDependency();
    }

    public List<Project> getDependencies() {
        return dependencies;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Project)) {
            return false;
        }

        Project project = (Project) obj;

        return name.equals(project.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
