package com.algorithm.various.graphs.build.build2;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Project {
    private ProjectState state = ProjectState.BLANK;
    private final String name;
    private final Set<Project> dependencies;

    public Project(String name) {
        this.name = name;
        this.dependencies = new HashSet<>();
    }

    public void addDependency(Project project) {
        dependencies.add(project);
    }

    public Collection<Project> getDependencies() {
        return dependencies;
    }

    public ProjectState getState() {
        return state;
    }

    public void setState(ProjectState state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Project)) {
            return false;
        }

        Project project = (Project) obj;
        return name.equals(project.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
