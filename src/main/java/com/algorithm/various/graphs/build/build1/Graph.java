package com.algorithm.various.graphs.build.build1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    private final Map<String, Project> projects;

    public Graph() {
        this.projects = new HashMap<>();
    }

    public void addProducts(String[][] projects) {
        for (String[] node : projects) {
            Project p1 = getProject(node[0]);
            Project p2 = getProject(node[1]);
            p1.addDependency(p2);
        }
    }

    private Project getProject(String name) {
        return projects.computeIfAbsent(name, Project::new);
    }

    public List<Project> getList() {
        return new ArrayList<>(projects.values());
    }
}
