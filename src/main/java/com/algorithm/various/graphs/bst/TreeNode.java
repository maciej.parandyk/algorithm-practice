package com.algorithm.various.graphs.bst;

public class TreeNode {
    private final int value;
    private TreeNode left;
    private TreeNode right;
    private TreeNode parent;

    public TreeNode(int value) {
        this.value = value;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
        left.setParent(this);
    }

    public void setRight(TreeNode right) {
        this.right = right;
        this.right.setParent(this);
    }

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }

    public int getValue() {
        return value;
    }

    void setParent(TreeNode node) {
        this.parent = node;
    }

    public TreeNode getParent() {
        return parent;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(value);
        builder.append("\n\tleft: "+left);
        builder.append("\n\tright: "+right+"\n");
        return builder.toString();
    }
}
