package com.algorithm.various.graphs.bst;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

public class MinimalBst {
    static TreeNode createMinimalBst(int[] values) {
        if (Objects.isNull(values)) {
            return null;
        }
        return divide(values, 0, values.length - 1);
    }

    private static TreeNode divide(int[] values, int start, int end) {
        if (end < start) {
            return null;
        }

        int pivot = (start + end) / 2;
        TreeNode n = new TreeNode(values[pivot]);
        n.setLeft(divide(values, start, pivot - 1));
        n.setRight(divide(values, pivot + 1, end));

        return n;
    }

    static List<List<Integer>> getLevels(TreeNode tree) {
        List<List<Integer>> list = new LinkedList<>();
        if (!Objects.isNull(tree)) {
            traverse(tree, list, 0);
        }

        return list;
    }

    private static void traverse(TreeNode node, List<List<Integer>> elements, int level) {
        if (Objects.isNull(node)) {
            return;
        }

        if (elements.size() <= level) {
            elements.add(level, new LinkedList<>());
        }

        elements.get(level).add(node.getValue());
        traverse(node.getLeft(), elements, level + 1);
        traverse(node.getRight(), elements, level + 1);
    }

    static List<Integer> breadthFirstList(TreeNode tree) {
        List<Integer> list = new LinkedList<>();
        if(Objects.isNull(tree)) {
            return list;
        }

        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.offer(tree);

        while (!queue.isEmpty()) {
            TreeNode n = queue.poll();
            list.add(n.getValue());

            if(!Objects.isNull(n.getLeft())) {
                queue.add(n.getLeft());
            }

            if (!Objects.isNull(n.getRight())) {
                queue.add(n.getRight());
            }
        }

        return list;
    }


    public static void main(String[] args) {
        int[] values = IntStream.range(0, 21).toArray();
        TreeNode tree = createMinimalBst(values);
        List<List<Integer>> list = getLevels(tree);

        System.out.println(list);

        List<Integer> result = breadthFirstList(tree);
        System.out.println(result);
    }
}
