package com.algorithm.various.graphs.bst;

import java.util.Objects;

/**
 * Next item in order - left->current->right. far most left node on right branch.
 */
public class NextItem {

    public static TreeNode findNextInOrder(TreeNode node) {
        if (Objects.isNull(node)) {
            return node;
        }

        if (!Objects.isNull(node.getRight())) {
            return farMostLeft(node.getRight());
        } else {
            TreeNode tmp = node;
            TreeNode n2 = node.getParent();
            while (!Objects.isNull(n2) && n2.getLeft() != tmp) {
                tmp = n2;
                n2 = n2.getParent();
            }

            return n2;
        }
    }

    private static TreeNode farMostLeft(TreeNode node) {
        if (Objects.isNull(node.getLeft())) {
            return node.getLeft();
        } else {
            TreeNode n = node.getLeft();
            while (!Objects.isNull(n.getLeft())) {
                n = n.getLeft();
            }
            return n;
        }
    }
}
