package com.algorithm.various.graphs.bst;

import java.util.Objects;
import java.util.stream.IntStream;

import static com.algorithm.various.graphs.bst.MinimalBst.createMinimalBst;
import static com.algorithm.various.graphs.bst.MinimalBst.getLevels;

public class BalancedTree {

    public static boolean isTreeBalanced(TreeNode tree) {
        if (Objects.isNull(tree)) {
            return false;
        }

        return maxDepth(tree.getLeft()) == maxDepth(tree.getRight());
    }

    private static int maxDepth(TreeNode n) {
        if (Objects.isNull(n)) {
            return 0;
        }

        if (!Objects.isNull(n.getLeft()) || !Objects.isNull(n.getRight())) {
            return maxDepth(n.getLeft()) + maxDepth(n.getRight()) + 1;
        }

        return 0;
    }

    public static void main(String[] args) {
        int[] values = IntStream.range(0, 21).toArray();
        TreeNode tree = createMinimalBst(values);

        System.out.println(isTreeBalanced(tree));

        values = IntStream.range(0, 6).toArray();
        tree = new TreeNode(5);
        tree.setLeft(new TreeNode(3));
        TreeNode n1 = new TreeNode(15);
        n1.setLeft(new TreeNode(10));
        n1.setRight(new TreeNode(16));
        tree.setRight(n1);
        System.out.println(getLevels(tree));

        System.out.println(isTreeBalanced(tree));
    }
}
