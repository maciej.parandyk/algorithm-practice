package com.algorithm.various.graphs.bst;

import java.util.Objects;
import java.util.stream.IntStream;

import static com.algorithm.various.graphs.bst.MinimalBst.createMinimalBst;
import static com.algorithm.various.graphs.bst.MinimalBst.getLevels;

public class ValidateBST {
    //validates whether tree is a bst: left less than root and right equal/greater that root

    public static boolean isValidBST(TreeNode node) {
        if (Objects.isNull(node)) {
            return false;
        }

        return validateBST(node);
    }

    private static boolean validateBST(TreeNode treeNode) {
        if (Objects.isNull(treeNode)) {
            return true;
        }

        boolean leftNodeValidity = checkLeft(treeNode.getLeft());
        boolean rightNodeValidity = checkRight(treeNode.getRight());

        if (leftNodeValidity && rightNodeValidity) {
            return validateBST(treeNode.getLeft()) && validateBST(treeNode.getRight());
        } else {
            return false;
        }
    }

    private static boolean checkLeft(TreeNode root) {
        if (Objects.isNull(root)) {
            return true;
        }

        return Objects.isNull(root.getLeft()) || root.getValue() > root.getLeft().getValue();
    }

    private static boolean checkRight(TreeNode root) {
        if (Objects.isNull(root)) {
            return true;
        }

        return Objects.isNull(root.getRight()) || root.getValue() <= root.getRight().getValue();
    }

    public static boolean validateBST2(TreeNode root) {
        if (Objects.isNull(root)) {
            return false;
        }

        return isBalanced(root, null, null);
    }

    private static boolean isBalanced(TreeNode node, Integer min, Integer max) {
        if (Objects.isNull(node)) {
            return true;
        }

        if ((!Objects.isNull(min) && node.getValue() < min) || (!Objects.isNull(max) && node.getValue() >= max)) {
            return false;
        }

        if ((!isBalanced(node.getLeft(), min, node.getValue()))
                || (!isBalanced(node.getRight(), node.getValue(), max))) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        int[] values = IntStream.range(0, 21).toArray();
        TreeNode tree = createMinimalBst(values);

        System.out.println(isValidBST(tree));
        System.out.println(validateBST2(tree));

        tree = new TreeNode(5);
        tree.setLeft(new TreeNode(3));
        TreeNode n1 = new TreeNode(15);
        n1.setLeft(new TreeNode(10));
        n1.setRight(new TreeNode(14));
        tree.setRight(n1);
        System.out.println(getLevels(tree));

        System.out.println(isValidBST(tree));

        System.out.println(validateBST2(tree));
    }
}
