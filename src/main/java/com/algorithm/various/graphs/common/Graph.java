package com.algorithm.various.graphs.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
    private final Map<String, TreeNode> nodes;

    public Graph() {
        nodes = new HashMap<>();
    }

    public List<TreeNode> getNodes() {
        return new ArrayList<>(nodes.values());
    }

    public void connect(String from, String to) {
        TreeNode nf = getNode(from);
        TreeNode nt = getNode(to);
        nf.addAdjacent(nt);
        nt.setParent(nf);
    }

    TreeNode getNode(String value) {
        return nodes.computeIfAbsent(value, TreeNode::new);
    }
}
