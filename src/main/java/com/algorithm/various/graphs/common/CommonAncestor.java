package com.algorithm.various.graphs.common;

import java.util.Objects;

public class CommonAncestor {

    public static TreeNode getAncestor(TreeNode n1, TreeNode n2) {
        int delta = getDepth(n1) - getDepth(n2);

        TreeNode fst = delta > 0 ? n2 : n1;
        TreeNode snd = delta > 0 ? n1 : n2;

        snd = goUp(snd, Math.abs(delta));

        while ((!Objects.isNull(fst) && !Objects.isNull(snd)) && !fst.equals(snd)) {
            fst = fst.getParent();
            snd = snd.getParent();
        }

        return fst;
    }

    private static int getDepth(TreeNode node) {
        int depth = 0;
        while(node != null){
            depth++;
            node = node.getParent();
        }

        return depth;
    }


    private static TreeNode goUp(TreeNode node, int depth) {

        while (depth > 0 && node != null) {
            depth--;
            node = node.getParent();
        }
        return node;
    }

    public static void main(String[] args) {
        String[][] nodesList = {
                {"c", "d"}, {"d","e"}, {"d", "f"}, {"d","g"}, {"e", "b"}, {"e","h"}, {"d","f"}, {"d","g"}, {"f", "i"},
                {"g", "j"}, {"g", "n"}, {"n", "o"}, {"j", "m"}, {"i","k"}, {"i","l"}
        };

        Graph graph = new Graph();
        for (String[] arg : nodesList) {
            graph.connect(arg[0], arg[1]);
        }

        TreeNode n1 = graph.getNode("l");
        TreeNode n2 = graph.getNode("b");
        TreeNode result = getAncestor(n1, n2);

        System.out.println(""+ n1+ " : "+n2 +"  => "+ result);

        n1 = graph.getNode("m");
        n2 = graph.getNode("o");

        result = getAncestor(n1,n2);

        System.out.println(""+ n1+ " : "+n2 +"  => "+ result);

        n1 = graph.getNode("m");
        n2 = graph.getNode("0");
        result = getAncestor(n1,n2);

        System.out.println(""+ n1+ " : "+n2 +"  => "+ result);
    }
}
