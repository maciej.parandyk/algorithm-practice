package com.algorithm.various.graphs.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TreeNode {
    private TreeNode parent;
    private final List<TreeNode> adjacent;
    private final String value;

    public TreeNode(String value) {
        this.value = value;
        this.adjacent = new ArrayList<>();
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public List<TreeNode> getAdjacent() {
        return adjacent;
    }

    public void addAdjacent(TreeNode node) {
        this.adjacent.add(node);
    }

    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (Objects.isNull(obj)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TreeNode)) {
            return false;
        }

        TreeNode node = (TreeNode) obj;

        return value.equals(node.value);
    }

    @Override
    public String toString() {
        return value;
    }
}
