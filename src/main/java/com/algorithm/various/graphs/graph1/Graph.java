package com.algorithm.various.graphs.graph1;

import java.util.*;

public class Graph {
    private Set<Node> nodes;

    public Graph() {
        nodes = new HashSet<>();
    }

    public void setNodes(Set<Node> nodes) {
        this.nodes = nodes;
    }

    public void join(Node n1, Node n2) {
        nodes.add(n1);
        n1.addAdjacent(n2);
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public boolean areNodesConnected(Node start, Node end) {
        if (start == end) {
            return false;
        }

        for (Node n : nodes) {
            n.resetNodes();
        }

        LinkedList<Node> queue = new LinkedList<>();
        start.setState(Node.State.VISITED);
        queue.offer(start);

        while (!queue.isEmpty()) {
            Node n = queue.poll();

            if (!Objects.isNull(n)) {
                for (Node nods : n.getAdjacent()) {
                    if (Node.State.UNVISITED == nods.getState()) {
                        if (nods == end) {
                            nods.setState(Node.State.VISITED);
                            return true;
                        }
                        queue.add(nods);
                    }
                }
            }
        }
        return false;
    }
}
