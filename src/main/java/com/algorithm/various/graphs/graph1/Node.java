package com.algorithm.various.graphs.graph1;

import java.util.HashSet;
import java.util.Set;

public class Node {
    public enum State {UNVISITED,VISITING, VISITED}
    private State state = State.UNVISITED;
    private final Set<Node> adjacents;

    public Node() {
        adjacents = new HashSet<>();
    }

    public void resetNodes() {
        unvisited();
        for (Node n : adjacents) {
            n.unvisited();
        }
    }

    private void unvisited() {
        setState(State.UNVISITED);
    }

    public void setState(State state) {
        this.state = state;
    }

    public Set<Node> getAdjacent() {
        return adjacents;
    }

    public void addAdjacent(Node node) {
        adjacents.add(node);
    }

    public void clear () {
        for (Node n : adjacents) {
            n.clear();
        }
        adjacents.clear();
    }

    public State getState() {
        return state;
    }
}
