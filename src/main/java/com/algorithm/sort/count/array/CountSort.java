package com.algorithm.sort.count.array;

/**
 * Count sort algorithm implementation for array types.
 */
public final class CountSort {
    private static final int MAX_SIZE = 255;

    private CountSort() {
    }

    /**
     * Count sort for array type.
     * @param values int array
     */
    public static void sort(int[] values) {
        int[] count = new int[MAX_SIZE];

        for (int i = 0; i < values.length; i++) {
            ++count[values[i]];
        }

        for (int i = 1; i < count.length; i++) {
            count[i] += count[i - 1];
        }

        int[] output = new int[values.length];

        for (int i = values.length - 1; i >= 0; i--) {
            output[--count[values[i]]] = values[i];
        }

        for (int i = 0; i < values.length; i++) {
            values[i] = output[i];
        }
    }
}
