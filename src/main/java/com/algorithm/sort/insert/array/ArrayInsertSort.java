package com.algorithm.sort.insert.array;

import java.util.List;

import static java.util.Collections.swap;

/**
 * Inser sort on array type.
 */
public final class ArrayInsertSort {
    private ArrayInsertSort() {
    }

    /**
     * Sort method on array input.
     *
     * @param values generic array
     * @param <T>    array item generic type
     */
    public static <T extends Comparable<T>> void sort(T[] values) {
        if (values == null || values.length <= 1) {
            return;
        }

        final int size = values.length;
        for (int i = 1; i < size; i++) {
            int j = i;
            while (j >= 1 && (values[j - 1].compareTo(values[j]) > 0)) {
                T tmp = values[j - 1];
                values[j - 1] = values[j];
                values[j] = tmp;
                j--;
            }
        }
    }

    /**
     * Sort method for list instance.
     *
     * @param list generic list
     * @param <T>  generic list item type
     */
    public static <T extends Comparable<T>> void sort(List<T> list) {
        if (list == null || list.size() <= 1) {
            return;
        }

        final int size = list.size();
        for (int i = 1; i < size; i++) {
            int j = i;
            while (j >= 1 && (list.get(j - 1).compareTo(list.get(j)) > 0)) {
                swap(list, j - 1, j);
                j--;
            }
        }
    }
}
