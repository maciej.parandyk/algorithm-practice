package com.algorithm.sort.quick.list;

import java.util.List;
import java.util.Random;

/**
 * Quick sort for generic list.
 */
public final class ListQuickSort {
    private static final Random RANDOM = new Random();
    private static final int INSERT_SORT_SIZE = 4;

    private ListQuickSort() {
    }

    /**
     * Array-based quick sort.
     * @param values sorted/unsorted generic array
     * @param <T> generic list item type
     */
    public static <T extends Comparable<T>> void sort(List<T> values) {
        if (values == null || values.size() <= 1) {
            return;
        }

        sort(values, 0, values.size());
    }

    private static <T extends Comparable<T>> void sort(List<T> values, int low, int high) {
        if (values == null || (high - low) <= 1) {
            return;
        }

        if ((high - low) <= INSERT_SORT_SIZE) {
            sortUsingSelectionSort(values, low, high);
            return;
        }

        int partition = partition(values, low, high);

        sort(values, low, partition);
        sort(values, partition + 1, high);
    }

    private static <T extends Comparable<T>> int partition(List<T> values, int low, int high) {
        //pivot randomization
        int pivot = RANDOM.nextInt((high - low) / 2) + low;

        final int right = high -1;
        swap(values,pivot, right);
        pivot = right;

        int i = low;
        for (int j = low; j < right; j++) {
            if (values.get(j).compareTo(values.get(pivot)) < 0) {
                swap(values, i++, j);
            }
        }

        swap(values, i, pivot);
        return i;
    }

    private static <T extends Comparable<T>> void swap(List<T> values, int first, int second) {

        T swaped = values.get(first);
        values.set(first, values.get(second));
        values.set(second, swaped);
    }

    private static <T extends Comparable<T>> void sortUsingSelectionSort(List<T> values, int low, int high) {

        for(int i = low; i < high - 1; i++) {
            int med = i;
            for (int j = i+1; j< high; j++) {
                if(values.get(med).compareTo(values.get(j)) > 0) {
                    med = j;
                }
            }
            swap(values, med, i);
        }
    }

}
