package com.algorithm.sort.quick.array;


import java.util.Random;

/**
 * Quick sort utilizing arrays.
 */
public final class ArrayQuickSort {
    private static final Random RANDOM = new Random();
    private static final int INSERT_SORT_SIZE = 4;

    private ArrayQuickSort() {
    }

    /**
     * Array-based quick sort.
     * @param values sorted/unsorted generic array
     * @param <T> array item generic type
     */
    public static <T extends Comparable<T>> void sort(T[] values) {
        if (values == null || values.length <= 1) {
            return;
        }

        sort(values, 0, values.length);
    }

    private static <T extends Comparable<T>> void sort(T[] values, int low, int high) {
        if (values == null || (high - low) <= 1) {
            return;
        }

        if((high - low) <= INSERT_SORT_SIZE) {
            selectionSort(values, low, high);
            return;
        }


        int partition = partition(values, low, high);

        sort(values, low, partition);
        sort(values, partition + 1, high);
    }

    private static <T extends Comparable<T>> int partition(T[] values, int low, int high) {
        //radomizing pivot selection
        int pivot = RANDOM.nextInt((high-low)/2) + low;

        swap(values, pivot, high - 1);
        pivot = high - 1;

        int i = low;
        for (int j = low; j < high; j++) {
            if (values[j].compareTo(values[pivot]) < 0) {
                swap(values, i++, j);
            }
        }

        swap(values, i, pivot);
        return i;
    }

    private static <T extends Comparable<T>> void swap(T[] values, int first, int second) {

        T swaped = values[first];
        values[first] = values[second];
        values[second] = swaped;
    }

    private static <T extends Comparable<T>> void selectionSort(T[] values, int low, int high) {

        for(int i=low; i < high -1; i++) {
            int med = i;
            for (int j= i + 1; j < high; j++) {
                if(values[med].compareTo(values[j]) > 0) {
                    med = j;
                }
            }
            swap(values, med, i);
        }
    }
}
