package com.algorithm.sort.bubble.array;

import java.util.List;

import static java.util.Collections.swap;

/**
 * Bubble Sort for array types.
 */
public final class ArrayBubbleSort {

    private ArrayBubbleSort() {
    }

    /**
     * Method sorting generic array.
     *
     * @param values generic table
     * @param <T>    generic type used for array
     */
    public static <T extends Comparable<T>> void sort(T[] values) {
        if (values == null || values.length <= 1) {
            return;
        }

        for (int j = values.length; j > 1; j--) {
            for (int i = 0; i < j-1; i++) {
                if (values[i].compareTo(values[i + 1]) > 0) {
                    T tmp = values[i];
                    values[i] = values[i + 1];
                    values[i + 1] = tmp;
                }
            }
        }
    }

    /**
     * Sort variation for generic list instance.
     *
     * @param collection a list
     * @param <T>        generic list item type
     */
    public static <T extends Comparable<T>> void sort(List<T> collection) {
        if (collection == null || collection.size() <= 1) {
            return;
        }

        for (int j = collection.size(); j > 1; j--) {
            for (int i = 0; i < j-1; i++) {
                if (collection.get(i).compareTo(collection.get(i + 1)) > 0) {
                    swap(collection,i, i+1);
                }
            }
        }
    }
}
