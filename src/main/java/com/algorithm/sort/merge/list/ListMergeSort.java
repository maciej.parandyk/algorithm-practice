package com.algorithm.sort.merge.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Merge sort on list.
 */
public final class ListMergeSort {

    private ListMergeSort() { }

    public static <T extends Comparable<T>> List<T> sort(List<T> values) {
        if (values == null || values.isEmpty()) {
            return values;
        }

        return sortMerge(values);
    }

    private static <T extends Comparable<T>> List<T> sortMerge(List<T> values) {
        if (values.size() == 1) {
            return values;
        }

        int pivot = values.size() / 2;

        List<T> h1 = new ArrayList<>(values.subList(0, pivot));
        List<T> h2 = new ArrayList<>(values.subList(pivot, values.size()));
        values.clear();
        h1 = sortMerge(h1);
        h2 = sortMerge(h2);

        //merging
        int h1x = 0;
        int h2x = 0;

        while (h1x < h1.size() && h2x < h2.size()) {
            if (h1.get(h1x).compareTo(h2.get(h2x)) < 0) {
                values.add(h1.get(h1x++));
            } else {
                values.add(h2.get(h2x++));
            }
        }

        while (h1x < h1.size()) {
            values.add(h1.get(h1x++));
        }

        while (h2x < h2.size()) {
            values.add(h2.get(h2x++));
        }

        return values;
    }
}
