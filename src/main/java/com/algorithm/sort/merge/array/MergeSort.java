package com.algorithm.sort.merge.array;

import java.util.Arrays;

/**
 * Merge sort on array implementation.
 */
public class MergeSort {


    public static <T extends Comparable<T>> T[] sort(T[] values) {
        if (values == null) {
            return values;
        }

        return sort(values, 0, values.length - 1);
    }

    private static <T extends Comparable<T>> T[] sort(T[] values, int low, int high) {
        if ((high - low) <= 0) {
            return values;
        }

        int pivot = (high + low) / 2;

        T[] h1 = Arrays.copyOfRange(values, low, pivot + 1);
        T[] h2 = Arrays.copyOfRange(values, pivot + 1, high + 1);
        h1 = sort(h1);
        h2 = sort(h2);

        //merging
        int idx = 0;
        int h1x = 0;
        int h2x = 0;

        while (h1x < h1.length && h2x < h2.length) {
            if (h1[h1x].compareTo(h2[h2x]) < 0) {
                values[idx++] = h1[h1x++];
            } else {
                values[idx++] = h2[h2x++];
            }
        }

        while (h1x < h1.length) {
            values[idx++] = h1[h1x++];
        }

        while (h2x < h2.length) {
            values[idx++] = h2[h2x++];
        }

        return values;
    }
}
