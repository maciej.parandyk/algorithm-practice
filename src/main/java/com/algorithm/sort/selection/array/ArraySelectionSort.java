package com.algorithm.sort.selection.array;

import java.util.List;

/**
 * Selection sort using array and list types.
 */
public final class ArraySelectionSort {

    private ArraySelectionSort() {
    }

    /**
     * Method for array types.
     * @param values generic input array
     * @param <T> array generic item type
     */
    public static <T extends Comparable<T>> void sort(T[] values) {
        if (values == null || values.length <= 1) {
            return;
        }

        for (int i = 0; i < values.length - 1; i++) {
            int mid = i;

            for (int j = i + 1; j < values.length; j++) {
                if (values[mid].compareTo(values[j]) > 0) {
                    mid = j;
                }
            }

            //swap
            T tmp = values[mid];
            values[mid] = values[i];
            values[i] = tmp;
        }
    }

    /**
     * Sort method on generic list input.
     * @param lista generic list
     * @param <T> generic list item type
     */
    public static <T extends Comparable<T>> void sort(List<T> lista) {
        if (lista == null || lista.size() <= 1) {
            return;
        }

        final int size = lista.size();
        for (int i = 0; i < size - 1; i++) {
            int mid = i;

            for (int j = i + 1; j < size; j++) {
                if (lista.get(mid).compareTo(lista.get(j)) > 0) {
                    mid = j;
                }
            }

            //swap
            T tmp = lista.get(mid);
            lista.set(mid, lista.get(i));
            lista.set(i, tmp);
        }
    }
}
