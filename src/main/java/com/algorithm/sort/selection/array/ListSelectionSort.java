package com.algorithm.sort.selection.array;

import java.util.Comparator;
import java.util.List;

import static java.util.Collections.swap;

/**
 * Selection sort algorithm for list.
 */
public class ListSelectionSort {

    /**
     * Sort method for list selection sort.
     * @param list a collection
     * @param <T> generic type content
     */
    public static <T extends Comparable<T>> void sort(List<T> list) {
        if(list == null || list.isEmpty()) {
            return;
        }

        final int size = list.size();
        for (int i = 0; i < size - 1; i++) {
            int mid = i;
            for (int j = i+1; j < size; j++) {
                if (list.get(mid).compareTo(list.get(j)) > 0) {
                    mid = j;
                }
            }
            swap(list,i, mid);
        }
    }
}
