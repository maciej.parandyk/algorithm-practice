package com.algorithm.sort.median;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.swap;

/**
 * Median sort implementation for list.
 */
public class ListMedianSort {

    /**
     * Sorts list elements using median sort.
     * @param list instance of a list
     * @param <T> generic item type
     */
    public static <T extends Comparable<T>> void sort(List<T> list) {
        if (list == null || list.isEmpty()) return;
        sortMedian(list, 0, list.size());
    }

    private static <T extends Comparable<T>> void sortMedian(List<T> list, int low, int high) {
        if((high - low) <= 1) return;
        
        final int median = medianPartition(list, low, high);
        sortMedian(list,low, median);
        sortMedian(list,median+1, high);
    }

    private static <T extends Comparable<T>> int medianPartition(List<T> list, int low, int high) {
        int med = (high + low) / 2;

        final int right = high -1;
        //move pivot to the end
        swap(list,right, med);
        med = right;
        final int size = (high-low);
        int idx = low;
        for(int i = low; i < right; i++) {
            if (list.get(i).compareTo(list.get(med)) < 0) {
                swap(list, i, idx++);
            }
        }

        swap(list,idx, med);
        return idx;
    }
}
