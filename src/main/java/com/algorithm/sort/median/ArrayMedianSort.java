package com.algorithm.sort.median;

import java.util.Arrays;

/**
 * Loose implementation of MedianSort ISA.
 */
public class ArrayMedianSort {
    /**
     * {@code sort()} method is a main entry for median sort functionality.
     * @param values generic array of classes implementing Comparable
     * @param <T> generic type
     */
    public static <T extends Comparable<? super T>> void sort(T[] values) {
        if(values == null) return;
        medianSort(values, 0, values.length);
    }

    private static <T extends Comparable<? super T>> void medianSort(T[] values, int low, int high) {
        if((high-low) <= 1) {
            return;
        }

        final int med = medianPartition(values,low, high);
        medianSort(values, low, med);
        medianSort(values, med+1, high);

    }

    //now it resembles quickstort
    private static <T extends Comparable<? super T>> int medianPartition(T[] values, int low, int high) {
        int med = (high + low) / 2;

        final int right = high -1;
        //move pivot to the end
        swap(values, right, med);
        med = right;
        final int size = (high-low);
        int idx = low;
        for(int i = low; i < right; i++) {
            if (values[i].compareTo(values[med]) < 0) {
                swap(values, i, idx++);
            }
        }

        swap(values,idx, med);
        return idx;
    }

    private static <T extends Comparable<? super T>> void swap(T[] values, int i, int j) {
        T t = values[i];
        values[i] = values[j];
        values[j] = t;
    }

}
