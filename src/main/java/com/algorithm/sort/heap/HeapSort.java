package com.algorithm.sort.heap;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Heap sort.
 */
public final class HeapSort {

    public static void heapIt(int[] values, Comparator<? super Integer> comp) {
        if (Objects.isNull(values)) {
            return;
        }

        int size = values.length;

        for (int i = size/2 -1; i>=0; i--) {
            heapfiy(comp, values, size, i);
        }

        for (int i= size -1; i>=0; i--) {
            swap(values, i, 0);
            heapfiy(comp, values, size, 0);
        }
    }

    private static void heapfiy(Comparator<? super Integer> comparator, int[] values, int size, int idx) {
        int largest = idx;
        int left = 2*idx + 1;
        int right = 2*idx +2;

        if (left < size && comparator.compare(values[left], values[largest]) > 0) {
            largest =  left;
        }

        if (right < size && comparator.compare(values[right], values[largest]) > 0) {
            largest = right;
        }

        if (largest != idx) {
            swap(values, idx, largest);
            heapfiy(comparator, values, size, largest);
        }
    }

    private static void swap(int[] values, int i1, int i2) {
        int val = values[i1];
        values[i1] = values[i2];
        values[i2] = val;
    }

    public static void main(String[] args) {
        int maxSize = 15;
        int[] values = IntStream.generate(() ->new Random().nextInt(maxSize)).limit(15).toArray();

        System.out.println(Arrays.toString(values));

        //max heap - by normal order
        heapIt(values, Comparator.naturalOrder());
        System.out.println(Arrays.toString(values));
    }
}
