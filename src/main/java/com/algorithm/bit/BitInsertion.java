package com.algorithm.bit;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class BitInsertion {

    public static int manipulate(int initVal, int insert, int j, int i) {
        int allOnes = ~0;
        int left = allOnes <<j+1;
        int right = ((1<<i)-1);

        int mask = left | right;

        return (initVal & mask) | (insert <<i);
    }

    public static void main(String[] args) {
        int initValue = 0;
        int insertable = 0;
        int beg = 0;
        int end = 0;
        try (Scanner scn = new Scanner(System.in, StandardCharsets.UTF_8)) {
            initValue = Integer.parseInt(scn.nextLine(), 2);
            System.out.println(Integer.toBinaryString(initValue));
            insertable = Integer.parseInt(scn.nextLine(), 2);
            System.out.println(Integer.toBinaryString(insertable));
            beg = scn.nextInt();
            end = scn.nextInt();
        }


        System.out.println(Integer.toBinaryString(manipulate(initValue,insertable,beg,end)));
    }
}
