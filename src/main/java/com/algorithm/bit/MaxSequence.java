package com.algorithm.bit;

import java.util.Scanner;

public class MaxSequence {

    public static int maxSequence(int value) {
        if (value == ~0) {
            return Integer.BYTES * 8;
        }

        int currentLength = 0;
        int previousLength = 0;
        int maxLength = 1;

        while (value != 0) {
            if ((value & 1) == 1) {
                currentLength++;
            } else {
                previousLength = (value & 2) == 0 ? 0 :currentLength;
                currentLength = 0;
            }

            value >>>= 1;
            maxLength = Math.max(currentLength + previousLength + 1, maxLength);
        }

        return maxLength;
    }

    public static void main(String[] args) {
        try (Scanner scn = new Scanner(System.in)) {
            int value = Integer.parseInt(scn.nextLine(), 2);
            System.out.println(maxSequence(value));
        }
    }
}
