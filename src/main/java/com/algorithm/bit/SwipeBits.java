package com.algorithm.bit;

import java.util.Scanner;

/**
 * Swips odd bits with even.
 */
public class SwipeBits {

    static int swap(int value) {
        return (((value & 0xaaaaaaaa) >>> 1) | ((value & 0x55555555) << 1));
    }

    public static void main(String[] args) {
        try (Scanner scn = new Scanner(System.in)) {
            int v1 = scn.nextInt();

            System.out.println(Integer.toBinaryString(v1));
            System.out.println(Integer.toBinaryString(swap(v1)));
        }
    }
}
