package com.algorithm.bit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ConvertToString {
    //accepts only values (0,1)
    public static String convertDoubleToString(double value) {
        if (value <= 0) {
            return "ERROR";
        }

        StringBuilder builder = new StringBuilder();
        if (!convertScaleIntoBinary(value, builder)
                || !convertPrecisionIntoBinary(value, builder)) {
            return "ERROR!";
        }

        return builder.toString();
    }

    private static boolean convertScaleIntoBinary(double value, StringBuilder builder) {
        int scale = (int) value;
        if (scale < 0) {
            builder.setLength(0);
            return false;
        }

        while(scale > 0) {
            int reminder = scale %2;
            scale /= 2;
            builder.insert(0, reminder);
        }
        return true;
    }

    private static boolean convertPrecisionIntoBinary(double value, StringBuilder builder) {
        if (value < 0) {
            return false;
        }

        double val = value - ((int) value);
        BigDecimal precision = BigDecimal.valueOf(val);

        if (precision.signum() > 0) {
            builder.append(".");
            BigDecimal fraction = BigDecimal.valueOf(0.5d);
            BigDecimal two = BigDecimal.valueOf(2l);
            while(precision.signum() > 0) {
                if (builder.substring(builder.indexOf(".")).length() > 32) {
                    return false;
                }


                if (precision.compareTo(fraction) >= 0) {
                    builder.append(1);
                    precision = precision.subtract(fraction);
                    precision = precision.setScale(8, RoundingMode.DOWN);
                } else {
                    builder.append(0);
                }
                fraction = fraction.divide(two);
            }
        }

        return true;
    }

    public static String convertDoubleToStringFrac(double val) {
        if (val >=1 || val <=0) {
            return "ERROR!";
        }

        StringBuilder sb = new StringBuilder("0.");

        double fraction = 0.5d;

        while (val > 0) {
            if (sb.length() > 32) {
                return "ERROR!";
            }

            if (val >= fraction) {
                sb.append(1);
                val -= fraction;
            } else {
                sb.append(0);
            }

            fraction /= 2;
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        //13.37  1101.01011110101110000101
        System.out.println(convertDoubleToString(13.37d));
        System.out.println(convertDoubleToStringFrac(13.37d));
    }
}
