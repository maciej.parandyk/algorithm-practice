package com.algorithm.bit;

import java.util.Scanner;

public class BitSequence {
    //brute force approach
    public static String bitSequenceStupidApproach(int value) {
        System.out.println(Integer.toBinaryString(value));
        int ones = countOnes(value);

        int lowest = findLowest(value, ones);
        int highest = findHighest(value, ones);

        return Integer.toBinaryString(lowest) +" "+lowest+ ":"+ highest +" "+ Integer.toBinaryString(highest);
    }

    private static int countOnes(int value) {
        int length = 0;
        while (value != 0) {
            if ((value & 1) == 1) {
                length++;
            }

            value >>>= 1;
        }

        return length;
    }

    private static int findLowest(int value, int count) {
        int onesCount = 0;
        while (count != onesCount) {
            value--;
            onesCount = countOnes(value);
        }

        return value;
    }

    private static int findHighest(int value, int count) {
        int onesCount = 0;
        while (count != onesCount) {
            value++;
            onesCount = countOnes(value);
        }

        return value;
    }

    //arithmetic approach
    static String bitSequenceArithmetic(int value) {
        int mask = ~0;
        int index = 0;

        //find smallest
        while ((value & (1<<index)) == 0) {
            index++;
        }

        mask <<=(index+1);

        int smallestValue = mask & value;
        System.out.println(Integer.toBinaryString(mask));
        if ((index-1) > 0) {
            smallestValue |= 1<<(index-1);
        }

        //find max
        int zeros = index;

        while ((value & (1<<index)) > 0) {
            index++;
        }

        mask = ~0 <<index;
        int highestValue = value & mask;
        highestValue |= 1<<index;

        index -= zeros;

        if ((index-1) >=0) {
            highestValue |= (1<<(index-1))-1;
        }


        return smallestValue+ ": "+ Integer.toBinaryString(smallestValue) + " : "+
                highestValue+ ": "+ Integer.toBinaryString(highestValue);
    }

    public static void main(String[] args) {
        try (Scanner scn = new Scanner(System.in)) {
            int value = scn.nextInt();

            System.out.println(bitSequenceStupidApproach(value));
            System.out.println(bitSequenceArithmetic(value));
        }
    }
}
