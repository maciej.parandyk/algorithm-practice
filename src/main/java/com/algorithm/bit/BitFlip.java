package com.algorithm.bit;

public class BitFlip {
    //how many bits you have to flip to make both numbers equal

    static int flipAmount(int v1, int v2) {
        int xor = v1 ^ v2;
        System.out.println(Integer.toBinaryString(v1));
        System.out.println(Integer.toBinaryString(v2));

        int count =0;
        while (xor > 0) {
            if ((xor & 1) == 1) {
                count++;
            }
            xor >>=1;
        }

        return count;
    }

    public static void main(String[] args) {
        System.out.println(flipAmount(19, 17));
    }
}
