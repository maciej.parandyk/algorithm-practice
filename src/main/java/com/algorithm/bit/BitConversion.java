package com.algorithm.bit;

import java.util.Scanner;

public class BitConversion {

    static int convertValues(int v1, int v2) {
        int count = 0;

        int xor = v1 ^ v2;

        while (xor > 0) {
            if ((xor & 1) == 1) {
                count++;
            }

            xor >>= 1;
        }

        return count;
    }

    public static void main(String[] args) {
        try (Scanner scn = new Scanner(System.in)) {
            int v1 = scn.nextInt();
            int v2 = scn.nextInt();

            System.out.println(Integer.toBinaryString(v1));
            System.out.println(Integer.toBinaryString(v2));
            System.out.println(convertValues(v1,v2));
        }
    }
}
